var gulp = require('gulp'),
    mocha = require('gulp-mocha'),
    plumber = require('gulp-plumber'),
    istanbul = require('gulp-istanbul'),
    isparta = require('isparta');

gulp.task('test', function() {
    return gulp.src('test/**/test.*.js')
        .pipe(mocha({reporter: 'nyan'}));
});

gulp.task('coverage', function(cb) {
    gulp.src('lib/**/*.js')
        .pipe(istanbul())
        .pipe(istanbul.hookRequire())
        .on('finish', function() {
            gulp.src('test/**/test.*.js')
                .pipe(mocha({reporter: 'nyan'}))
                .pipe(istanbul.writeReports())
                .on('end', cb);
        });
});
