var express = require('express'),
    path = require('path'),
    cons = require('consolidate'),
    async = require('async'),
    _ = require('underscore');

var methodOverride = require('method-override'),
    bodyParser = require('body-parser'),
    // csrf = require('csurf'),
    multer = require('multer'),
    cookieParser = require('cookie-parser');

var Preload = require('./preload'),
    handler = require('./handler'),
    logger = require('./logger').loggers.get('stickyrice');

var CONFIG = {
    url: '*',
    host: 'localhost',
    port: 9000,

    viewPath: 'views',
    viewEngine: 'dust',

    staticPath: 'public'
};

var setDefault = function() {
    var defaultConfig = JSON.parse(JSON.stringify(CONFIG));
    for (var key in this._config) {
        defaultConfig[key] = this._config[key];
    }

    return defaultConfig;
}

var StickyRice = function(baseDir, config) {
    GLOBAL.__basedir = baseDir || process.cwd();

    this._app = express();
    this._router = new express.Router();
    this._modules = [];
    this._controllers = {};
    this._routes = [];
    this._filters = {};
    this._config = config || setDefault();
};

StickyRice.prototype._setupRoute = function () {
    var router = this._router;
    var routes = this._routes;
    var controllers = this._controllers;
    var filters = this._filters;

    for (var key in controllers) {
        controllers[key] = new controllers[key]();
    };

    for (var key in filters) {
        filters[key] = new filters[key]();
    };

    var routeSize = routes.length;
    for (var i = 0; i < routeSize; i++) {
        var route = routes[i];
        var controller = controllers[route.controller];
        var tmpFilter = route.filters || [];
        var tmpFilterSize = tmpFilter.length;
        for (var j = 0; j < tmpFilterSize; j++) {
            tmpFilter[j] = filters[tmpFilter[j]];
        };
        var callback = handler.wrap(controller[route.fn], tmpFilter, controller);
        router[route.method](route.url, callback);
    };
}

StickyRice.prototype._setupExpress = function () {
    var app = this._app;
    var config = this._config;
    var modules = this._modules;
    var router = this._router;

    app.engine(config.viewEngine, cons[config.viewEngine]);

    app.set('views', path.join(__basedir, config.viewPath));
    app.set('view engine', config.viewEngine);

    app.use(methodOverride());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(multer());
    app.use(cookieParser());

    var moduleSize = modules.length;
    for (var index = 0; index < moduleSize; index++) {
        var module = modules[index];
        app.use(module);
    }

    app.use(router);
    app.use(express.static(path.join(__basedir, config.staticPath)));
}

StickyRice.prototype.addModule = function (module) {
    this._modules.push(module);
};

StickyRice.prototype.addRoute = function (route) {
    this._routes.push(route);
};

StickyRice.prototype.addController = function (name, controller) {
    var isHasController = _.has(this._controllers, name);
    if (!isHasController) {
        this._controllers[name] = controller;
    }
};

StickyRice.prototype.addFilter = function (name, filter) {
    var isHasFilter = _.has(this._filters, name);
    if (!isHasFilter) {
        this._filters[name] = filter;
    }
};

StickyRice.prototype.getHostConfig = function () {
    return this._config.host;
};

StickyRice.prototype.getPortConfig = function () {
    return this._config.port;
};

StickyRice.prototype.getViewPathConfig = function () {
    return this._config.viewPath;
};

StickyRice.prototype.getViewEngineConfig = function () {
    return this._config.viewEngine;
};

StickyRice.prototype.getStaticPathConfig = function () {
    return this._config.staticPath;
};

StickyRice.prototype.getModulesSize = function () {
    return this._modules.length;
};

StickyRice.prototype.setPreload = function (preload) {
    this._preload = preload;
};

StickyRice.prototype.run = function() {
    var self = this;
    async.waterfall([
        function runPreload (next) {
            if (self._preload) {
                self._preload.load(function () {
                    next();
                });            
            }
            else {
                next();
            }
        },
        function runExpress () {
            self._setupRoute();
            self._setupExpress();
            self._app.listen(self._config.port, self._config.host, function() {
                logger.info('Start server host name: ' + self._config.host);
                logger.info('User port: ' + self._config.port);
            });
        }
    ]);
};

exports = module.exports = StickyRice;