var async = require('async'),
    path = require('path'),
    fs = require('fs'),
    util = require('util');

var logger = require('./logger').loggers.get('stickyrice');

var Preload = function (baseDir, config) {
    if (!baseDir) throw new Error('Preload require base directory');
    this.baseDir = baseDir;
    this.config = config;
}

Preload.prototype.load = function(done) {
    var self = this;
    var preloads = this.config.preloads || [];
    var config = this.config;

    async.forEachOfSeries(preloads, function (name, key, next) {        
        var name = path.extname(name) || util.format("%s.js", name);
        var modulePath = path.join(self.baseDir, 'preloads', name);                
        var preload = require(modulePath);           

        try {
            preload.setup(config, next);    
        } catch (e) {
            logger.error(e.stack);
            next(new Error(util.format('Preload "%s" is not have a setup function.', modulePath)));    
        }
    }, function (error) {
        if (error) {
            throw error;
        }

        done();
    });
};

module.exports = Preload;