var express = require('express'),
    handler = require('./handler'),
    util = require('util'),
    _ = require('underscore');

var Router = function () {
    this.router = new express.Router();
};

Router.prototype.path = function (path, method, fn, filters, controller) {
    filters = filters || [];
    var callback = handler.wrap(fn, filters, controller);
    this.router[method](path, callback);  

    return this;  
};

Router.prototype.get = function () {
    return this.router;
};

module.exports = Router;