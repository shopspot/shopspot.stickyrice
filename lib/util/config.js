var fs = require('fs'),
    _ = require('underscore');

var argv = require('optimist').argv;

var config = {};

module.exports = {
    parse: function (config) {
        if (argv.env) {
            var envArgv = config[argv.env];
            delete config[argv.env];

            if (_.isArray(envArgv)) {
                var envConfig = envArgv.pop();

                var configs = _.map(envArgv, function (configName) { return config[configName] });
                configs.push(envConfig);
                configs = [config].concat(configs);

                config = _.extend.apply(_, configs);
            }
            else {
                config = _.extend(config, envArgv); 
            }
        }

        return config;
    },

    get: function () {
        return config;
    }
}
