var path = require('path'),
    util = require('util'),
    _ = require('underscore'),
    fs = require('fs');

var logger = require('../logger').loggers.get('stickyrice');

var loadModules = function (directory, files) {
    try {
        var directories = directory.split('/');
        var dirSize = directories.length;
        var fullDirectory = __basedir;

        for (var i = 0; i < dirSize; i++) {
            fullDirectory = path.join(fullDirectory, directories[i]);    
        };
    }
    catch (e) {
        throw new Error(e);
    }
        
    var listModule = [];

    if (!files || !Array.isArray(files) || files.length === 0) {
        throw new Error('Invalid files.');
    }
    for (var i = 0; i < files.length; i++) {
        try {
            var modulePath = path.join(fullDirectory, files[i]);
            var name = path.extname(modulePath) || util.format("%s.js", modulePath);
            var module = require(name);
            listModule.push(module);
        }
        catch (e) {
            logger.error(e.stack);
            throw new Error(util.format('Modules %s will be ignore because it\'s not found', files[i]));
        }
    };

    return listModule;
};

module.exports = loadModules;