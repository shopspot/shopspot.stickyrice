var _ = require('underscore'),
    util = require('util'),
    configuration = require('./util/config');

function render (req, res, options) {
    res._render = res.render;
    return function (view, context, options) {
        context = context || {};
        options = options || {};

        var defaultContext = {
            cache: options.cache
        };

        context = _.extend(defaultContext, context);

        res._render(view, context, function (err, html) {
            res.setHeader('Content-Type', 'text/html');
            res.send(html);
        });
    }
};

function json (req, res) {
    res._json = res.json;
    return function (json, options) {
        options = options || {};
        var status = options.status || 200;

        res.status(status)._json(json);
    }
}

function text (req, res) {
    return function (text, options) {
        options = options || {};

        res.setHeader('Content-Type', 'text/plain');
    }
};

exports.wrap = function (fn, filters, context) {
    var self = this;
    var config = configuration.get();

    if (!fn) throw new Error('Function fn is empty');

    var wrapper = _.wrap(fn, function () {
        fn = Array.prototype.shift.call(arguments);
        var req = Array.prototype.slice.call(arguments, 0)[0];
        var res = Array.prototype.slice.call(arguments, 1)[0];

        filters = filters || [];

        var isBreak = false;
        var filterCount = filters.length;

        for (var index = 0; index < filterCount; index++) {
            var filter = filters[index];
            var result = filter['invoke'].apply(filter, arguments);

            isBreak = (result === undefined || result === true) ? false : true;
            if (isBreak) {
                break;
            }
        }

        if (isBreak === true) {
            return ;
        }

        res.render = render(req, res, {
            cache: config['cacheView']
        });
        res.json = json(req, res);
        res.text = text(req, res);

        var result = null;
        result = fn.apply(context, arguments);

        return result;
    });

    return wrapper;
};
