var _ = require('underscore'),
    winston = require('winston');

winston.loggers.add('stickyrice', {
    transports: [
        new (winston.transports.Console) ({
            name: 'info-log',
            level: 'info',
            colorize: true,
            timestamp: true
        })
    ]
});

function parse (logConfig) {
    logConfig = logConfig || {};
    logConfig.server = logConfig.server || "server.log";
    logConfig.error = logConfig.error || "error.log";
    var loggers = logConfig.container || {};
    var pathName = (logConfig.fileOption && logConfig.fileOption.path) ? logConfig.fileOption.path: '';

    if (logConfig.colors) {
        winston.addColors(logConfig.colors);
    }

    if (logConfig.file) {
        var srLog = winston.loggers.get('stickyrice');
        if (!logConfig.console) {
            srLog.remove('info-log');
        }
        srLog.add(winston.transports.File, {
            name: 'info-file',
            filename: pathName + logConfig.server,
            level: 'info',
            handleExceptions: true,
            prettyPrint: true,
            json: false
        });
        srLog.add(winston.transports.File, {
            name: 'error-file',
            filename: pathName + logConfig.error,
            level: 'error',
            handleExceptions: true,
            prettyPrint: true,
            json: false
        });
    }

    for (var key in loggers) {
        var logger = loggers[key];
        var transports = [];
        if (logConfig.console) {
            transports.push(new (winston.transports.Console) ({
                level: logger,
                colorize: true,
                timestamp: true
            }));
        }

        if (logConfig.file) {
            transports.push(new (winston.transports.File) ({
                filename: pathName + key + '.log',
                level: logger,
                json: false
            }));
        }

        winston.loggers.add(key, {
            transports: transports
        });
    }
};

module.exports = {
    parse: parse,
    loggers: winston.loggers
};
