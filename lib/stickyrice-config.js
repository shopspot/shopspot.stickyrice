var path = require('path'),
    _ = require('underscore');

var Builder = require('./stickyrice-builder'),
    Preload = require('./preload'),
    logger = require('./logger'),
    loadModules = require('./util/load-modules');

var srLogger = require('./logger').loggers.get('stickyrice');

var Method = {
    'get': 'get',
    'post': 'post',
    'put': 'put',
    'delete': 'delete'
};

var StickryriceConfig = function (baseDir) {
    this.baseDir = baseDir;
    this.builder = new Builder(baseDir);
    this.loadModules = loadModules;
    this.logger = logger;
};

StickryriceConfig.prototype._parseFilter = function (filters) {
    var baseDir = this.baseDir;
    var builder = this.builder
    var filterSize = filters.length;

    for (var i = 0; i < filterSize; i++) {
        var filterName = filters[i];
        var filterPath = path.join(baseDir, 'filters', filterName);
        try {
            var filter = require(filterPath);
            builder.filter(filterName, filter);
        } catch (e) {
            var error = 'Can not found filter : ' + filterName;
            srLogger.error(error)
            throw e;
        }
    };  
}

StickryriceConfig.prototype._parseController = function (controllerName) {
    var baseDir = this.baseDir;
    var builder = this.builder;

    var controllerPath = path.join(baseDir, 'controllers', controllerName);
    try {
        var controller = require(controllerPath);
        builder.controller(controllerName, controller);
    } catch (e) {
        var error = 'Can not found controller : ' + controllerName;
        srLogger.error(error)
        throw e;
    }
}

StickryriceConfig.prototype._parseRoute = function (routeConfig)  {
    var self = this;
    var baseDir = this.baseDir;
    var builder = this.builder;

    _.chain(routeConfig).keys().each(function (key) {
        var config = routeConfig[key];
        var url = key.substr(key.indexOf(':') + 1);
        var method = Method[key.substr(0, key.indexOf(':'))];
        var controllerName = config.controller.substr(0, config.controller.indexOf('.'));
        var fn = config.controller.substr(config.controller.indexOf('.') + 1);
        var filters = config.filters || [];
        self._parseController(controllerName);
        self._parseFilter(filters);

        builder.route()
                    .url(url)
                    .method(method)
                    .controller(controllerName)
                    .fn(fn)
                    .filters(filters)
                .commit()
    });
}

StickryriceConfig.prototype.parse = function (config) {
    var builder = this.builder;
    if (config.loggers) {
        this.logger.parse(config.loggers);
    }

    builder.host(config.host)
        .port(config.port)
        .viewPath(config.viewPath)
        .viewEngine(config.viewEngine)
        .staticPath(config.staticPath)

    if (config.routes) {
        this._parseRoute(config.routes);
    }

    if (config.externals) {
        var directory = config.externals.directory || 'externals';
        var modulesConfig = config.externals.modules;
        var modules = this.loadModules(directory, modulesConfig);
        var moduleSize = modules.length;

        for (var i = 0; i < moduleSize; i++) {
            builder.module(modules[i](config.externals[modules[i].NAME]));
        };
    }

    if (config.preloads) {
        var dir = this.baseDir
        var preload = new Preload(dir, config);
        builder.preload(preload);
    } 

    return this;
};

StickryriceConfig.prototype.build = function () {
    return this.builder.build();
};

module.exports = StickryriceConfig;