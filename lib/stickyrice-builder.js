var _ = require('underscore')
    path = require('path');

var StickyRice = require('./stickyrice'),
    srLogger = require('./logger').loggers.get('stickyrice');

var Builder = function (baseDir) {
    this._baseDir = baseDir;
    this._config = {};
    this._sr = new StickyRice(baseDir);
};

Builder.prototype.host = function (host) {
    this._sr._config.host = host || this._sr._config.host;
    return this;
};

Builder.prototype.port = function (port) {
    this._sr._config.port = port || this._sr._config.port;
    return this;
};

Builder.prototype.viewPath = function (viewPath) {
    this._sr._config.viewPath = viewPath || this._sr._config.viewPath;
    return this;
};

Builder.prototype.viewEngine = function (viewEngine) {
    this._sr._config.viewEngine = viewEngine || this._sr._config.viewEngine;
    return this;
};

Builder.prototype.staticPath = function (staticPath) {
    this._sr._config.staticPath = staticPath || this._sr._config.staticPath;
    return this;
};

Builder.prototype.controller = function (name, controller) {
    this._sr.addController(name, controller);
    return this;
};

Builder.prototype.filter = function (name, filter) {
    this._sr.addFilter(name, filter);
    return this;
};

Builder.prototype.route = function () {
    var self = this;
    var route = {
        url: '',
        method: '',
        controller: '',
        fn: '',
        filters: []
    };

    return {
        url: function (url) {
            route.url = url;
            return this;
        },
        method: function (method) {
            route.method = method;
            return this;
        },
        controller: function (controller) {
            route.controller = controller;
            return this;
        },
        fn: function (fn) {
            route.fn = fn;
            return this;
        },
        filters: function (filters) {
            route.filters = filters;
            return this;
        },
        commit: function() {
            self._sr.addRoute(route);
            return self;
        }
    }
}

Builder.prototype.module = function (module) {
    this._sr.addModule(module);
    return this;
};

Builder.prototype.preload = function (preload) {
    this._sr.setPreload(preload);
    return this;
};

Builder.prototype.build = function () {
    return this._sr;
};

module.exports = Builder;