var hook = require('./util/hook'),
        path = require('path'),
        util = require('util'),
    should = require('should');

var handler = require('../lib/handler.js');

describe('handler', function () {
    describe('#wrap()', function () {
        it('should error coz miss fn', function (done) {
            (function () { handler.wrap() }).should.throw();

            done();
        });

        it('should stop require when filters return false', function (done) {
            var req = {};
            var res = {
                render: function () {},
                json: function () {},
                send: function () {},
                setHeader: function () {} 
            };
            
            var fn = function () {};
            var filters = [
                {'invoke': function () {return false}},
                {'invoke': function () {}}
            ];
            var wrapper;
            (function () { wrapper = handler.wrap(fn, filters) }).should.not.throw();
            wrapper(req, res);

            done();
        });

        it('should success without filters', function (done) {
            var req = {};
            var res = {
                render: function () {},
                json: function () {},
                send: function () {},
                setHeader: function () {} 
            };
            var fn = function () {};
            var filters = [];
            var wrapper;
            (function () { wrapper = handler.wrap(fn, filters) }).should.not.throw();
            wrapper(req, res);

            done();
        });

        it('should success with filters', function (done) {
            var req = {};
            var res = {
                render: function () {},
                json: function () {},
                send: function () {},
                setHeader: function () {} 
            };
            var fn = function () {};
            var filters = [
                {'invoke': function () {return true}},
                {'invoke': function () {}}
            ];
            var wrapper;
            (function () { wrapper = handler.wrap(fn, filters) }).should.not.throw();
            wrapper(req, res);

            done();
        });

        it('should success with respon json', function (done) {
            var req = {};
            var res = {
                render: function () {},
                json: function () {},
                send: function () {},
                setHeader: function () {},
                status: function () { return this }
            };
            var fn = function (req, res) { res.json() };
            var filters = [];
            var wrapper;
            (function () { wrapper = handler.wrap(fn, filters) }).should.not.throw();
            wrapper(req, res);

            done();
        });

        it('should success with respon render', function (done) {
            var req = {};
            var res = {
                render: function () {},
                json: function () {},
                send: function () {},
                setHeader: function () {} 
            };
            var fn = function (req, res) { res.render() };
            var filters = [];
            var wrapper;
            (function () { wrapper = handler.wrap(fn, filters) }).should.not.throw();
            wrapper(req, res);

            done();
        });

        it('should success with respon test', function (done) {
            var req = {};
            var res = {
                render: function () {},
                json: function () {},
                send: function () {},
                setHeader: function () {} 
            };
            var fn = function (req, res) { res.text() };
            var filters = [];
            var wrapper;
            (function () { wrapper = handler.wrap(fn, filters) }).should.not.throw();
            wrapper(req, res);

            done();
        });
    });
});