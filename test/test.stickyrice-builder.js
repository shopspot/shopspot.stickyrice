var hook = require('./util/hook'),
    should = require('should'),
    sinon = require('sinon'),
    express = require('express');

var Builder = require('../lib/stickyrice-builder.js');
var builder;

describe('StickyRiceBuilder', function () {
    before(function (done) {
        var baseDir = path.join(process.cwd(), 'test', 'mocks', 'server');
        builder = new Builder(baseDir);
        builder._sr._app = express();
        done();
    });

    describe('#host()', function () {
        it('should success', function (done) {
            var host = 'shopspotapp.com';
            builder.host(host)
                .build()
                .getHostConfig().should.equal(host);

            done();
        });
    });

    describe('#port()', function () {
        it('should success', function (done) {
            var port = '1150';
            builder.port(port)
                .build()
                .getPortConfig().should.equal(port);

            done();
        });
    });

    describe('#viewPath()', function () {
        it('should success', function (done) {
            var viewPath = 'vi';
            builder.viewPath(viewPath)
                    .build()
                    .getViewPathConfig().should.equal(viewPath);

            done();
        });
    });

    describe('#viewEngine()', function () {
        it('should success', function (done) {
            var viewEngine = 'ddust';
            builder.viewEngine(viewEngine)
                    .build()
                    .getViewEngineConfig().should.equal(viewEngine);

            done();
        });
    });

    describe('#staticPath()', function () {
        it('should success', function (done) {
            var staticPath = 'private';
            builder.staticPath(staticPath)
                    .build()
                    .getStaticPathConfig().should.equal(staticPath);

            done();
        });
    });

    describe('#route()', function () {
        it('should return object function', function (done) {
            var obj = builder.route();
            
            obj.should.have.property('url');
            obj.should.have.property('method');
            obj.should.have.property('controller');
            obj.should.have.property('filters');
            obj.should.have.property('commit');

            done();
        });
    });

    describe('#module()', function () {
        it('should success', function (done) {
            var _module = {};

            var moduleSize = builder.build()
                                    .getModulesSize();
            builder.module(_module)
                    .build()
                    .getModulesSize().should.above(moduleSize)

            done();
        });
    });

    describe('#preload()', function () {
        it('should success', function (done) {
            var sr = builder._sr;

            var srSetPreloadStib = sinon.stub(sr, 'setPreload');
            var preload = {};
            builder.preload(preload);

            srSetPreloadStib.called.should.be.true();
            srSetPreloadStib.restore();

            done();
        });
    })

    describe('#build()', function () {
        it('should success', function (done) {
            builder.build().should.have.property('run');
            
            done();
        });
    });
});