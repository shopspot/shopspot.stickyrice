var async = require('async'),
    should = require('should')
    sinon = require('sinon'),
    path = require('path');

var hook = require('./util/hook'),
    Config = require('../lib/stickyrice-config.js');

var srConfig;

describe('Config', function () {
    before(function (done) {
        var baseDir = process.cwd();
        var currentDir = path.join(baseDir, 'test', 'mocks', 'server');
        srConfig = new Config(currentDir);

        done();
    });

    describe('#_parseFilter()', function () {
        it('should error coz not found filter', function (done) {
            var filters = ['aaaa'];
            (function () {
                srConfig._parseFilter(filters);
            }).should.throw();

            done();
        });

        it('should success', function (done) {
            var filters = ['User'];
            var builderFilterStub = sinon.stub(srConfig.builder, 'filter');

            srConfig._parseFilter(filters);

            builderFilterStub.called.should.be.true;
            builderFilterStub.restore();

            done();
        });
    });

    describe('#_parseController()', function () {
        it('should error coz not found controller', function (done) {
            var controller = 'aaaa';
            (function () {
                srConfig._parseController(controller);
            }).should.throw();

            done();
        });

        it('should success', function (done) {
            var controller = 'Test2';
            var builderControllerStub = sinon.stub(srConfig.builder, 'controller');

            srConfig._parseController(controller);

            builderControllerStub.called.should.be.true;
            builderControllerStub.restore();

            done();
        });
    });

    describe('#_parseRoute()', function () {
        it('should success', function (done) {
            var config = {
                'get:/': { 'controller': 'Test2.get' , 'filters': ['User'] }
            };

            (function () {
                srConfig._parseRoute(config);    
            }).should.not.throw();

            done();
        });
    });

    describe('#parse()', function () {
        it('should success', function (done) {
            var config = {
                loggers: {
                    file: false,
                    console: true
                },
                filters: ['User'],
                host: 'localhost',
                port: '1234',
                viewPath: 'views',
                viewEngine: 'dust',
                staticPath: 'public',
                externals: {
                    modules: ['ex1'],
                    ex1: {}
                },
                routes: {
                    'get:/': { 'controller': 'Test2.get' , 'filters': ['User'] }
                },
                preloads: ['setup']
            };

            var builder = srConfig.builder;
            var logger = srConfig.logger;

            var stubLoggerParse = sinon.stub(logger, 'parse');
            var stubBuilderHost = sinon.stub(builder, 'host').returns(builder);
            var stubBuilderPort = sinon.stub(builder, 'port').returns(builder);
            var stubBuilderViewPath = sinon.stub(builder, 'viewPath').returns(builder);
            var stubBuilderViewEngine = sinon.stub(builder, 'viewEngine').returns(builder);
            var stubBuilderStaticPath = sinon.stub(builder, 'staticPath').returns(builder);
            var stubConfigParseRoute = sinon.stub(srConfig, '_parseRoute');
            var stubLoadModule = sinon.spy(srConfig, 'loadModules');
            var stubBuilderModule = sinon.stub(builder, 'module').returns(builder);
            var stubBuilderPreload = sinon.stub(builder, 'preload').returns(builder);

            srConfig.parse(config);
            stubLoggerParse.called.should.be.true();
            stubLoggerParse.restore();

            stubBuilderHost.called.should.be.true();
            stubBuilderHost.restore();

            stubBuilderPort.called.should.be.true();
            stubBuilderPort.restore();

            stubBuilderViewPath.called.should.be.true();
            stubBuilderViewPath.restore();

            stubBuilderViewEngine.called.should.be.true();
            stubBuilderViewEngine.restore();

            stubBuilderStaticPath.called.should.be.true();
            stubBuilderStaticPath.restore();

            stubConfigParseRoute.called.should.be.true();
            stubConfigParseRoute.restore();

            stubLoadModule.called.should.be.true();
            stubLoadModule.restore();

            stubBuilderModule.called.should.be.true();
            stubBuilderModule.restore();

            stubBuilderPreload.called.should.be.true();
            stubBuilderPreload.restore();

            done();
        });
    });

    describe('#build', function () {
        it('should build success', function (done) {
            var builder = srConfig.builder;
            var buildStub = sinon.stub(builder, 'build');

            srConfig.build();

            buildStub.called.should.be.true();
            buildStub.restore();

            done();
        });
    })
})