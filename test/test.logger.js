var logger = require('../lib/logger.js'),
    should = require('should');

describe('logger', function () {
    describe('#parse', function () {
        it('should parse config to container', function (done) {
            var loggerConfig = {
                colors: {
                    info: 'green',
                    error: 'red'
                },
                console: true,
                container: {
                    'aaa': 'info'
                }
            };

            logger.parse(loggerConfig);
            var testLogger;

            (function () {
                testLogger = logger.loggers.get('stickyrice');
            }).should.not.throw();
            testLogger.should.be.ok();

            done();
        });

        it('should add new container logger', function (done) {
            var loggerConfig = {
                colors: {
                    info: 'green',
                    error: 'red'
                },
                console: false,
                file: true,
                fileOption: {
                    path: './test/'
                },
                container: {
                    'test': 'info'
                }
            };

            logger.parse(loggerConfig);
            var testLogger;

            (function () {
                testLogger = logger.loggers.get('test');
            }).should.not.throw();
            testLogger.should.be.ok();

            done();
        });
    });

    describe('#loggers', function () {
        it('should have stickyrice logger', function (done) {
            var srLogger;
            (function () {
                srLogger = logger.loggers.get('stickyrice');
            }).should.not.throw();
            srLogger.should.be.ok();

            done();
        });
    });
});