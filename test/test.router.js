var hook = require('./util/hook'),
    path = require('path'),
    should = require('should'),
    util = require('util'),
    express = require('express');

var Router = require('../lib/router.js');

describe('Router', function () {
    describe('#get', function () {
        it('should success', function (done) {
            var router;

            (function () {
                router = new Router().get();
            }).should.not.throw();

            router.should.be.ok();

            done();
        });
    });

    describe('#path', function (done) {
        it('should error coz blank parametor', function (done) {
            var router;

            (function () {
                router = new Router().path();
            }).should.throw();

            done();
        });

        it('should error coz invalid method', function (done) {
            var router,
                    path = '/',
                    method = 'aaaa',
                    fn = function () {},
                    filters = [];

            (function () {
                router = new Router().path();
            }).should.throw();

            done();
        });

        it('should success', function (done) {
            var router,
                    path = '/',
                    method = 'get',
                    fn = function () {},
                    filters = ['User'];

            (function () {
                router = new Router().path(path, method, fn, filters)
                                                            .path(path, method, fn, filters);
            }).should.not.throw();
            
            router.router.stack.length.should.equal(2);
            done();
        });
    });
});