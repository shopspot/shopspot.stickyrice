var hook = require('./util/hook'),
    sinon = require('sinon'),
    should = require('should'),
    express = require('express');

var StickyRice = require('../lib/stickyrice.js');

describe('StickyRice', function() {

    describe('#addModule()', function () {
        it('should success', function (done) {
            var sr = new StickyRice();
            var size = sr._modules.length;
            sr.addModule('test');
            sr.getModulesSize().should.above(size);
            done();
        });
    });

    describe('#constructor()', function() {
        it('with basic config should be success', function () {
            (function() {
                var sr = new StickyRice();
            }).should.not.throw();
        });
    });

    describe('#run()', function() {
        it('should bind config port', function (done) {
            var sr = new StickyRice();
            var app = sr._app;

            var appListenStub = sinon.stub(app, 'listen');

            (function() {
                sr.run();
            }).should.not.throw();

            setTimeout(function () {
                appListenStub.called.should.be.true();
                appListenStub.restore();        

                done();
            }, 0);

        })

        it('should success with preload', function (done) {
            var sr = new StickyRice();
            var module = function (req, res, next) {};
            var preload = {
                load: function (cb) {
                    cb();
                }
            };

            sr.setPreload(preload);
            sr.addModule(module);
            var app = sr._app;

            var appListenStub = sinon.stub(app, 'listen');
            var srSetupRouteStub = sinon.stub(sr, '_setupRoute');
            var srSetupExpressStub = sinon.stub(sr, '_setupExpress');

            (function() {
                sr.run();
            }).should.not.throw();

            setTimeout(function () {
                srSetupRouteStub.called.should.be.true();
                srSetupRouteStub.restore();  

                srSetupExpressStub.called.should.be.true();
                srSetupExpressStub.restore();  
                
                appListenStub.called.should.be.true();
                appListenStub.restore();        

                done();
            }, 0);

        })

    });

});
