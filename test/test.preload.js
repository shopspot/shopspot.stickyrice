var hook = require('./util/hook'),
    path = require('path'),
    util = require('util'),
    should = require('should');

var Preload = require('../lib/preload.js');
var baseDir = process.cwd();
var cb = function () {};

describe('preload', function () {
    it('should error coz no exists folder preloads', function (done) {
        var config = {
            preloads: ['aaa']
        };

        var preload = new Preload(baseDir, config);
        (function () { preload.load(cb) }).should.throw();

        done();
    });

    it('should error coz not have file', function (done) {
        var config = {
            preloads: ['aaa']
        };
        var currentDir = path.join(baseDir, 'test', 'mocks', 'server');
        var preload = new Preload(currentDir, config);
        (function () { preload.load(cb) }).should.throw();

        done();
    });

    it('should error coz not have setup function', function (done) {
        var config = {
            preloads: ['noSetup']
        };
        var currentDir = path.join(baseDir, 'test', 'mocks', 'server');
        var preload = new Preload(currentDir, config);
        (function () { preload.load(cb) }).should.throw();

        done();
    });

    it('should error coz not a file', function (done) {
        var config = {
            preloads: ['1']
        };
        var currentDir = path.join(baseDir, 'test', 'mocks', 'server');
        var preload = new Preload(currentDir, config);
        (function () { preload.load(cb) }).should.throw();

        done();
    });


    it('should success', function (done) {
        var config = {
            preloads: ['setup']
        };
        var currentDir = path.join(baseDir, 'test', 'mocks', 'server');
        var preload = new Preload(currentDir, config);
        (function () { preload.load(cb) }).should.not.throw();

        done();
    });
});