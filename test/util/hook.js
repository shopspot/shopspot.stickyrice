exports = module.exports = function(obj) {
    if (obj.hook || obj.unkook) {
        throw new Error('Object already has method hook and unhook');
    }

    obj.hook = function(method, hooker, isAsync) {
        var self = this,
            refMethod;

        if (Object.prototype.toString.call(self[method]) !== '[object Function]') {
            throw new Error('No method name : ' + method);
        }

        if (self.unhook.methods[method]) {
            throw new Error('Method already hooked: ' + method);
        }

        refMethod = (self.unhook.methods[method] = self[method]);

        self[method] = function() {
            var args = Array.prototype.slice.call(arguments);

            while(args.length < refMethod.length) {
                args.push(undefined);
            }

            args.push(function() {
                var args = arguments;

                if (isAsync) {
                    process.nextTick(function() {
                        refMethod.apply(self, args);
                    })
                }
                else {
                    refMethod.apply(self, args);
                }
            });

            hooker.apply(self, args);
        }
    }

    obj.unhook = function(methodName) {
        var self = this,
        ref = self.unhook.methods[methodName];

        if (ref) {
            self[methodName] = self.unhook.methods[methodName];
            delete self.unhook.methods[methodName];
        }
        else {
            throw new Error('Method not hooked: ' + methodName);
        }
    }

    obj.unhook.methods = {};
}
