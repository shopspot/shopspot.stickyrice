var should = require('should'),
    path = require('path');

var loadModule = require('../../lib/util/load-modules');

describe('TestFunctionalLoadModules', function () {
    before(function (done) {
        GLOBAL.__basedir = process.cwd();
        done();
    });

    describe('#loadModules()', function () {
        it('should error coz missing directory', function (done) {
            (function () { loadModule() }).should.throw();
            done();
        });

        it('should error coz invalid directory', function (done) {
            var directory = 'aaa';
            (function () { loadModule(directory) }).should.throw();
            done();
        });

        it('should error coz missing files', function (done) {
            var directory = 'test/mocks/server/controllers';
            (function () { loadModule(directory) }).should.throw();
            done();
        });

        it('should error coz files is not array', function (done) {
            var directory = 'test/mocks/server/controllers';
            var files = 'hello';
            (function () { loadModule(directory, files) }).should.throw('Invalid files.');
            done();
        });

        it('should error coz files is empty array', function (done) {
            var directory = 'test/mocks/server/controllers';
            var files = [];
            (function () { loadModule(directory, files) }).should.throw('Invalid files.');
            done();
        });

        it('should error coz not found files', function (done) {
            var directory = 'controllers';
            var files = ['aaa'];
            (function () { loadModule(directory, files) }).should.throw();
            done();
        });

        it('should success', function (done) {
            var directory = 'test/mocks/server/controllers';
            var files = ['Test2'];
            var results;
            (function () { results = loadModule(directory, files) }).should.not.throw();
            results.length.should.equal(results.length);
            done();
        });
    });
});