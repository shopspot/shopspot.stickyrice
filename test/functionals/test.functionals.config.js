var should = require('should'),
    path = require('path'),
    fs = require('fs'),
    _ = require('underscore'),
    configuration = require('../../lib/util/config');

describe('config', function () {
    describe('#get', function () {
        it('should success', function () {
            var result = configuration.get();
            result.should.containDeep({});
        });
    });
    
    describe('#parse', function () {
        it('should success without envelopments', function (done) {
            var result = configuration.parse({});
            result.should.containDeep({});

            done(); 
        });

        it('should success with envelopments', function (done) {
            var argv = require('optimist').argv;
            argv.env = 'dev';

            var config = {
                port: 1,
                dev: {
                    port: 2
                }
            };

            var result = configuration.parse(config);
            result.port.should.equal(2);

            done(); 
        });

        it('should success with multi envelopments', function (done) {
            var argv = require('optimist').argv;
            argv.env = 'test';

            var config = {
                port: 1,
                dev: {
                    port: 2,
                    routes: [ '1', '2', '3']
                },
                test: ['dev', {
                    routes: ['4', '5']
                }]
            };

            var result = configuration.parse(config);

            done(); 
        });
    });
});