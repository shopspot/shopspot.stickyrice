module.exports = exports = {
  StickyRice: require('./lib/stickyrice.js'),
  Builder: require('./lib/stickyrice-builder.js'),
  Config: require('./lib/stickyrice-config.js'),
  loggers: require('./lib/logger').loggers,
  Preload: require('./lib/preload.js')
};